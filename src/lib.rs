#![feature(plugin)]
#![plugin(clippy)]
#![feature(iter_arith)]

extern crate arrayvec;
extern crate ordered_float;

pub mod gst;
pub mod rtree;
