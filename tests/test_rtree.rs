extern crate gst;
use gst::rtree::{RTree, Rect};

#[test]
fn test_rtree_basic_insert_get() {
    let mut tree = RTree::new();
    tree.insert(Rect::from_float(0., 0., 0., 0.), String::from("Origin"));
    assert_eq!(tree.get(&Rect::from_float(0., 0., 0., 0.))[0].1, "Origin");
}

#[test]
fn test_rtree_inserts_triggering_splits() {
    let mut rtree = RTree::new();
    for i in -10..10 {
        for j in -10..10 {
            let r = Rect::from_float(i as f32, i as f32, j as f32, j as f32);
            let s = format!("{}^2 + {}", i, j);
            rtree.insert(r, s);
        }
    }

    assert_eq!(rtree.get(&Rect::from_float(-10., 10., -10., 10.)).len(), 400);
}


#[test]
fn test_gst_get_rect() {
    let mut rtree = RTree::new();
    rtree.insert(Rect::from_float(0., 0., 0., 0.), String::from("Origin"));
    rtree.insert(Rect::from_float(1., 1., 0., 0.), String::from("X1"));
    rtree.insert(Rect::from_float(0., 0., 1., 1.), String::from("Y1"));
    rtree.insert(Rect::from_float(1., 1., 1., 1.), String::from("X1Y1"));
    assert_eq!(rtree.get(&Rect::from_float(0.0,  0.5,  0.0,  0.5 )).len(), 1);
    assert_eq!(rtree.get(&Rect::from_float(0.0,  1.0,  0.0,  1.0 )).len(), 4);
    assert_eq!(rtree.get(&Rect::from_float(0.25, 0.75, 0.25, 0.75)).len(), 0);
}

